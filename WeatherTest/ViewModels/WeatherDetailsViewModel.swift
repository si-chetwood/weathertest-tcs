//
//  WeatherDetailsViewModel.swift
//  WeatherTest
//
//  Created by Simon on 28/02/19.
//  Copyright © 2019 TCS. All rights reserved.
//

import Foundation

final class WeatherDetailsViewModel {
    
    private var weather: Weather!
    
    init(with weather: Weather) {
        self.weather = weather
    }
    
    var generalDescription: String { return self.weather.details?.first?.description ?? "-" }
    var iconURL: URL? {
        // The URL should be part of web service class. but that can be improved later.
        if let iconName = self.weather.details?.first?.iconName,
            let url = URL(string: "http://openweathermap.org/img/w/\(iconName).png") {
            return url
        }
        return nil
    }
    var temperatureString: String { return self.string(from: self.weather.main?.temperature, unit: "C") }
    var pressureString: String { return self.string(from: weather.main?.pressure, unit: "hPa") }
    var humidityString: String { return self.string(from: weather.main?.humidity, unit: "%") }
    var minimumTemperatureString: String { return self.string(from: weather.main?.minimumTemperature, unit: "C") }
    var maximumTemperatureString: String { return self.string(from: weather.main?.maximumTemperature, unit: "C") }
    var seaLevelString: String { return self.string(from: weather.main?.seaLevel, unit: "hPa") }
    var groundLevelString: String { return self.string(from: weather.main?.groundLevel, unit: "hPa") }
    var windSpeedString: String { return self.string(from: weather.wind?.speed, unit: "meter/sec") }
    var windDegreesString: String { return self.string(from: weather.wind?.degrees, unit: "degrees") }
    var cloudlinessString: String { return self.string(from: weather.clouds?.all, unit: "%") }
    var last1HourRainString: String { return self.string(from: weather.rain?.last1Hour, unit: "mm") }
    var last1HourSnowString: String { return self.string(from: weather.snow?.last1Hour, unit: "mm") }
    
    private func string(from double: Double?, unit: String) -> String {
        if let double = double {
            let formatter = NumberFormatter()
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 1
            if let string = formatter.string(from: NSNumber(value: double)) {
                return "\(string) \(unit)"
            }
        }
        return "- \(unit)"
    }
    
    private func string(from int: Int?, unit: String) -> String {
        if let int = int {
            return "\(int) \(unit)"
        }
        return "- \(unit)"
    }
}
