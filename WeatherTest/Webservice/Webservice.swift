//
//  Webservice.swift
//  WeatherTest
//
//  Created by Simon on 28/02/19.
//  Copyright © 2019 TCS. All rights reserved.
//

import Foundation

class Webservice {
    
    struct CurrentWeatherAPIResponse: Decodable {
        var weather: Weather?
    }
    
    func getWeatherDetails(lat: Double, long: Double, completion: @escaping (Weather?) -> ()) -> URLSessionDataTask? {
        
        // this URL should not be put directly here. Should be a using a custom URL builder class. But, let's not make this complex for now.
        guard let url = URL(string: "https://openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(long)&appid=b6907d289e10d714a6e88b30761fae22&units=metric") else { return nil }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response
            , error) in
            guard let data = data else { return }
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode(Weather.self, from: data)
                let weather = response //.weather
                DispatchQueue.main.async {
                    completion(weather)
                }
            } catch let err {
                print("Err - weather details web service", err)
                completion(nil)
            }
        }
        task.resume()
        return task
    }
}
