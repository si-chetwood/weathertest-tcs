//
//  Weather.swift
//  WeatherTest
//
//  Created by Simon on 28/02/19.
//  Copyright © 2019 TCS. All rights reserved.
//

import Foundation

struct Weather: Decodable {
 
    private enum CodingKeys: String, CodingKey {
        case details = "weather"
        case main, wind, clouds, rain, snow
    }
    
    struct Details: Decodable {
        private enum CodingKeys: String, CodingKey {
            case description
            case iconName = "icon"
        }
        
        var description: String?
        var iconName: String?
    }
    
    struct Main: Decodable {
        private enum CodingKeys: String, CodingKey {
            case temperature = "temp"
            case pressure, humidity
            case minimumTemperature = "temp_min"
            case maximumTemperature = "temp_max"
            case seaLevel = "sea_level"
            case groundLevel = "grnd_level"
        }
        
        var temperature: Double?
        var pressure: Double?
        var humidity: Double?
        var minimumTemperature: Double?
        var maximumTemperature: Double?
        var seaLevel: Double?
        var groundLevel: Double?
        
        public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.temperature = try container.decodeIfPresent(Double.self, forKey: .temperature)
            self.pressure = try container.decodeIfPresent(Double.self, forKey: .pressure)
            self.humidity = try container.decodeIfPresent(Double.self, forKey: .humidity)
            self.minimumTemperature = try container.decodeIfPresent(Double.self, forKey: .minimumTemperature)
            self.maximumTemperature = try container.decodeIfPresent(Double.self, forKey: .maximumTemperature)
            self.seaLevel = try container.decodeIfPresent(Double.self, forKey: .seaLevel)
            self.groundLevel = try container.decodeIfPresent(Double.self, forKey: .groundLevel)
        }
        
        init() {
        }
    }
    
    struct Cloud: Decodable {
        var all: Int?
    }
    
    struct Wind: Decodable {
        private enum CodingKeys: String, CodingKey {
            case speed
            case degrees = "deg"
        }
        
        var speed: Double?
        var degrees: Double?
    }
    
    struct Rain: Decodable {
        private enum CodingKeys: String, CodingKey {
            case last1Hour = "1h"
            case last3Hours = "3h"
        }
        
        var last1Hour: Double?
        var last3Hours: Double?
    }
    
    struct Snow: Decodable {
        private enum CodingKeys: String, CodingKey {
            case last1Hour = "1h"
            case last3Hours = "3h"
        }
        
        var last1Hour: Double?
        var last3Hours: Double?
    }
    
    var details: [Details]?
    var main: Main?
    var wind: Wind?
    var clouds: Cloud?
    var rain: Rain?
    var snow: Snow?
}
