//
//  ViewController.swift
//  WeatherTest
//
//  Created by Simon on 28/02/19.
//  Copyright © 2019 TCS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var latitudeTextField: UITextField!
    @IBOutlet weak var longitudeTextField: UITextField!
    @IBOutlet weak var activityIndicatorView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var weatherDetailStackView: UIStackView!
    @IBOutlet weak var weatherIconImageView: UIImageView!
    @IBOutlet weak var currentWeatherDescriptionLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var maximumTemperatureLabel: UILabel!
    @IBOutlet weak var minimumTemperatureLabel: UILabel!
    @IBOutlet weak var seaLevelLabel: UILabel!
    @IBOutlet weak var groundLevelLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDegreesLabel: UILabel!
    @IBOutlet weak var cloudlinessLabel: UILabel!
    @IBOutlet weak var rainStackView: UIStackView!
    @IBOutlet weak var last1HourRainLabel: UILabel!
    @IBOutlet weak var snowStackView: UIStackView!
    @IBOutlet weak var last1HourSnowLabel: UILabel!
    
    var viewModel: WeatherDetailsViewModel! {
        didSet { self.display(viewModel: self.viewModel) }
    }
    var task: URLSessionDataTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator.stopAnimating()
        self.weatherDetailStackView.isHidden = true
        
        self.latitudeTextField.delegate = self
        self.longitudeTextField.delegate = self
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // Just an example of how we can cancel the existing/unfinished REST API request
        self.task?.cancel()
    }
    
    @IBAction func loadWeatherDetailsButtonTapped(_ sender: UIButton) {
        guard let latString = self.latitudeTextField.text, let lat = Double(latString),
            let longString = self.longitudeTextField.text, let long = Double(longString) else {
            // Incorrect input
            return
        }
        // Call web service here
        self.activityIndicatorView.isHidden = false
        self.weatherDetailStackView.isHidden = true
        self.activityIndicator.startAnimating()
        self.task = Webservice().getWeatherDetails (lat: lat, long: long) { [unowned self] (weather) in
            guard let weather = weather else {
                return
            }
            self.viewModel = WeatherDetailsViewModel(with: weather)
        }
    }
    
    private func display(viewModel: WeatherDetailsViewModel) {
        self.currentWeatherDescriptionLabel.text = viewModel.generalDescription
        // Set icon
        if let url = viewModel.iconURL, let data = try? Data(contentsOf: url) {
            self.weatherIconImageView.image = UIImage(data: data)
        }
        
        self.temperatureLabel.text = viewModel.temperatureString
        self.pressureLabel.text = viewModel.pressureString
        self.humidityLabel.text = viewModel.humidityString
        self.minimumTemperatureLabel.text = viewModel.minimumTemperatureString
        self.maximumTemperatureLabel.text = viewModel.maximumTemperatureString
        self.seaLevelLabel.text = viewModel.seaLevelString
        self.groundLevelLabel.text = viewModel.groundLevelString
        self.windSpeedLabel.text = viewModel.windSpeedString
        self.windDegreesLabel.text = viewModel.windDegreesString
        self.cloudlinessLabel.text = viewModel.cloudlinessString
        self.last1HourRainLabel.text = viewModel.last1HourRainString
        self.last1HourSnowLabel.text = viewModel.last1HourSnowString
        
        self.activityIndicator.stopAnimating()
        self.activityIndicatorView.isHidden = true
        self.weatherDetailStackView.isHidden = false
        
        self.scrollView.flashScrollIndicators()
    }
    
    
}

extension ViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
