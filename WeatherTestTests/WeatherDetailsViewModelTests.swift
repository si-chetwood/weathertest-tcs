//
//  WeatherTestTests.swift
//  WeatherTestTests
//
//  Created by Simon on 28/02/19.
//  Copyright © 2019 TCS. All rights reserved.
//

import XCTest
@testable import WeatherTest

class WeatherDetailsViewModelTests: XCTestCase {

    var sut: WeatherDetailsViewModel!
    
    override func setUp() {
        sut = WeatherDetailsViewModel(with: self.getMockWeather())
    }

    private func getMockWeather() -> Weather {
        var weather = Weather()
        weather.details = [ Weather.Details(description: "Clear sky", iconName: "01d") ]
        
        var main = Weather.Main()
        main.temperature = 25.1
        main.pressure = 1013
        main.humidity = 24
        main.minimumTemperature = 22
        main.maximumTemperature = 27
        main.seaLevel = 1011
        main.groundLevel = 1000
        weather.main = main
        
        weather.wind = Weather.Wind(speed: 1, degrees: 12)
        weather.clouds = Weather.Cloud(all: 10)
        weather.rain = Weather.Rain(last1Hour: 11, last3Hours: 33)
        weather.snow = Weather.Snow(last1Hour: 22, last3Hours: 44)
        return weather
    }
    
    override func tearDown() {
        sut = nil
    }

    func testStringGenerations() {
        XCTAssertEqual(sut.generalDescription, "Clear sky")
        XCTAssertEqual(sut.iconURL, URL(string: "http://openweathermap.org/img/w/01d.png")!)
        XCTAssertEqual(sut.temperatureString, "25.1 C")
        XCTAssertEqual(sut.pressureString, "1013 hPa")
        XCTAssertEqual(sut.humidityString, "24 %")
        XCTAssertEqual(sut.minimumTemperatureString, "22 C")
        XCTAssertEqual(sut.maximumTemperatureString, "27 C")
        XCTAssertEqual(sut.seaLevelString, "1011 hPa")
        XCTAssertEqual(sut.groundLevelString, "1000 hPa")
        XCTAssertEqual(sut.windSpeedString, "1 meter/sec")
        XCTAssertEqual(sut.windDegreesString, "12 degrees")
        XCTAssertEqual(sut.cloudlinessString, "10 %")
        XCTAssertEqual(sut.last1HourRainString, "11 mm")
        XCTAssertEqual(sut.last1HourSnowString, "22 mm")
    }
}
